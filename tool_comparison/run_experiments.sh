#!/bin/bash
cd "$(dirname "$0")"


results=()
for solution in solutions/*/ ; do
  for scenario in scenarios/*/ ; do
    docker build $scenario -f $solution/Dockerfile
    results+=("$scenario---$solution---$? ")
  done
done

printf "%s\n" ${results[*]}


