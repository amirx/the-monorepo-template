from setuptools import setup, find_packages
import pathlib

current_proj_path = pathlib.Path(__file__).parent.parent.resolve()

setup(
    name='app_a',
    version='1.0.0',
    author='amir',
    description='pkg',
    packages=find_packages(),
    install_requires=[f'lib_a @ file://{current_proj_path}/lib_a'],
)